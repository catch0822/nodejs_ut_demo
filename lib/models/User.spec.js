'use strict';

var _chai = require('chai');

var _sinon = require('sinon');

var _sinon2 = _interopRequireDefault(_sinon);

var _path = require('path');

var _path2 = _interopRequireDefault(_path);

var _mongoose = require('mongoose');

var _mongoose2 = _interopRequireDefault(_mongoose);

var _Constant = require('../constant/Constant');

var _Constant2 = _interopRequireDefault(_Constant);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var sut = new require('./User').userModel();

describe('Test DB', function () {
    var userFindOneMock = void 0;
    var testUser = {
        name: 'test',
        age: '20',
        idcard: 'C221115555'
    };

    before(function () {
        userFindOneMock = _sinon2.default.stub(_mongoose2.default.Model, 'findOne');
    });

    after(function () {
        userFindOneMock.restore();
    });

    beforeEach(function () {});

    afterEach(function () {});

    it('mongo.getUserByIdcard, should return user name and age.', function () {
        // Mock prepare
        userFindOneMock.withArgs({ idcard: testUser.idcard }).yields(null, testUser);
        // Trigger sut
        var callback = _sinon2.default.spy();
        sut.getUserByIdcard(testUser.idcard, callback);
        // Expectation
        (0, _chai.assert)(callback.calledOnce);
        var spyUSER = callback.firstCall.args[1];
        (0, _chai.expect)(spyUSER.name).to.equal(testUser.name);
        (0, _chai.expect)(spyUSER.age).to.equal(testUser.age);
    });

    it('mongo.getTicketPrice, should return the final price.', function () {
        var discount = 100;
        var result = sut.getTicketPrice(discount);
        // Expectation
        (0, _chai.expect)(result).to.equal(_Constant2.default - discount);
    });
});