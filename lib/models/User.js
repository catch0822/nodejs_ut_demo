'use strict';

var _log = require('log');

var _log2 = _interopRequireDefault(_log);

var _mongoose = require('mongoose');

var _mongoose2 = _interopRequireDefault(_mongoose);

var _Constant = require('../constant/Constant');

var _Constant2 = _interopRequireDefault(_Constant);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var log = new _log2.default('userModel.js');

var Schema = _mongoose2.default.Schema;

var userSchema = new Schema({
    name: {
        type: String,
        required: true
    },
    idcard: {
        type: String,
        index: { unique: true, dropDups: true },
        required: true
    },
    age: {
        type: Number,
        required: true
    }
});

userSchema.static('getUserByIdcard', function (idcard, callback) {
    UserModel.findOne({
        idcard: idcard
    }, function (err, user) {
        callback(err, user);
    });
});

userSchema.static('getTicketPrice', function (discount) {
    return _Constant2.default - discount;
});

var UserModel = _mongoose2.default.model('user', userSchema);

function User() {}

User.userModel = function () {
    return UserModel;
};

module.exports = User;