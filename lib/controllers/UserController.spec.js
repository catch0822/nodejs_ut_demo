'use strict';

var _chai = require('chai');

var _sinon = require('sinon');

var _sinon2 = _interopRequireDefault(_sinon);

var _UserController = require('./UserController');

var _UserController2 = _interopRequireDefault(_UserController);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var UserModel = new require('../models/User').userModel();

// controller test; test the discount logic, so I have to mock the db
describe('Test Controller; test the discount logic', function () {
    var getUserByIdcardStub = void 0;
    var getTicketPriceStub = void 0;
    before(function () {
        getUserByIdcardStub = _sinon2.default.stub(UserModel, 'getUserByIdcard');
        getTicketPriceStub = _sinon2.default.stub(UserModel, 'getTicketPrice');
    });

    after(function () {
        getUserByIdcardStub.restore();
        getTicketPriceStub.restore();
    });

    it('1. C258622151, age:31, the discount should be $230', function () {
        // Mock DB preparation
        var requestBody = { id: 'C258622151' };
        var user = {
            name: 'test_230',
            age: '31',
            idcard: requestBody.id
        };
        var expectDiscount = 230;
        var expectPrice = 270;
        getUserByIdcardStub.withArgs(requestBody.id).yields(null, user);
        getTicketPriceStub.withArgs(expectDiscount).returns(expectPrice);
        // Trigger sut
        _UserController2.default.getUserTicketPrice(requestBody.id, function (res) {
            (0, _chai.expect)(res.discount).to.equal(expectDiscount);
            (0, _chai.expect)(res.ticketPrice).to.equal(expectPrice);
        });
    });
});

// controller test; test the discount logic, so I have to mock the db
describe('[Async] Test Controller; test the discount logic', function () {
    var getUserByIdcardStub = void 0;
    var getTicketPriceStub = void 0;
    before(function () {
        getUserByIdcardStub = _sinon2.default.stub(UserModel, 'getUserByIdcard');
        getTicketPriceStub = _sinon2.default.stub(UserModel, 'getTicketPrice');
    });

    after(function () {
        getUserByIdcardStub.restore();
        getTicketPriceStub.restore();
    });

    beforeEach(function () {});

    afterEach(function () {});

    it('1. C258622151, age:31, the discount should be $230', function () {
        // Mock DB preparation
        var requestBody = { id: 'C258622151' };
        var user = {
            name: 'test_230',
            age: '31',
            idcard: requestBody.id
        };
        var expectDiscount = 230;
        var expectPrice = 270;
        getUserByIdcardStub.withArgs(requestBody.id).yields(null, user);
        getTicketPriceStub.withArgs(expectDiscount).returns(expectPrice);
        // Trigger sut
        return new Promise(function (resolve, reject) {
            _UserController2.default.getUserTicketPrice(requestBody.id, function (res) {
                resolve(res);
            });
        }).then(function (res) {
            (0, _chai.expect)(res.discount).to.equal(expectDiscount);
            (0, _chai.expect)(res.ticketPrice).to.equal(expectPrice);
        });
    });

    it('2. F321113052, age:31, 身分證字號不合法', function () {
        // Mock DB preparation
        var requestBody = { id: 'F321113052' };
        var response = '身分證字號不合法';

        // Trigger sut
        return new Promise(function (resolve, reject) {
            _UserController2.default.getUserTicketPrice(requestBody.id, function (res) {
                resolve(res);
            });
        }).then(function (res) {
            (0, _chai.expect)(res.err).to.equal(response);
        });
    });

    it('3. 使用者不存在', function () {
        // Mock DB preparation
        var requestBody = { id: 'C258622151' };
        var expectUser = null;
        var response = '使用者不存在';
        getUserByIdcardStub.withArgs(requestBody.id).yields(null, expectUser);
        // Trigger sut
        return new Promise(function (resolve, reject) {
            _UserController2.default.getUserTicketPrice(requestBody.id, function (res) {
                resolve(res);
            });
        }).then(function (res) {
            (0, _chai.expect)(res.err).to.equal(response);
        });
    });

    it('4. Error', function () {
        // Mock DB preparation
        var requestBody = { id: 'C258622151' };
        var err = 'stub error';
        getUserByIdcardStub.withArgs(requestBody.id).yields(err, {});
        // Trigger sut
        return new Promise(function (resolve, reject) {
            _UserController2.default.getUserTicketPrice(requestBody.id, function (res) {
                resolve(res);
            });
        }).then(function (res) {
            (0, _chai.expect)(res.err).to.equal(err);
        });
    });
});