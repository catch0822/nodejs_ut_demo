'use strict';

var _path = require('path');

var _path2 = _interopRequireDefault(_path);

var _log = require('log');

var _log2 = _interopRequireDefault(_log);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var log = new _log2.default('UserController.js');
var UserModel = new require(_path2.default.join('..', 'models', 'User.js')).userModel();

var checkTwID = function checkTwID(id) {
    var city = new Array(1, 10, 19, 28, 37, 46, 55, 64, 39, 73, 82, 2, 11, 20, 48, 29, 38, 47, 56, 65, 74, 83, 21, 3, 12, 30);
    id = id.toUpperCase();
    if (id.search(/^[A-Z](1|2)\d{8}$/i) == -1) {
        return false;
    } else {
        id = id.split('');
        var total = city[id[0].charCodeAt(0) - 65];
        for (var i = 1; i <= 8; i++) {
            total += eval(id[i]) * (9 - i);
        }
        total += eval(id[9]);
        return total % 10 == 0;
    }
};

exports.getUserTicketPrice = function (idcard, callback) {
    var isLegal = checkTwID(idcard);
    if (!isLegal) {
        return callback({ err: "身分證字號不合法" });
    }

    UserModel.getUserByIdcard(idcard, function (err, user) {
        if (err) {
            log.error(err);
            return callback({ err: err });
        }

        if (!user) {
            callback({ err: "使用者不存在" });
        } else {
            var initDiscont = 0;
            var userIdCard = user.idcard;
            var userAge = user.age;
            if (userIdCard.charAt(1) == '2') {
                //女生優惠100塊
                initDiscont += 100;
            }
            if (userIdCard.charAt(9) % 2 != 0) {
                //身分證最後一碼為奇數者優惠50塊
                initDiscont += 50;
            }
            if (userAge > 30) {
                //年齡超過30優惠80塊
                initDiscont += 80;
            }
            var ticketPrice = UserModel.getTicketPrice(initDiscont);
            callback({ discount: initDiscont, ticketPrice: ticketPrice });
        }
    });
};

var checkTwIDAsync = function checkTwIDAsync(id) {
    return new Promise(function (resolve, reject) {
        var city = new Array(1, 10, 19, 28, 37, 46, 55, 64, 39, 73, 82, 2, 11, 20, 48, 29, 38, 47, 56, 65, 74, 83, 21, 3, 12, 30);
        id = id.toUpperCase();
        if (id.search(/^[A-Z](1|2)\d{8}$/i) == -1) {
            resolve(false);
        } else {
            id = id.split('');
            var total = city[id[0].charCodeAt(0) - 65];
            for (var i = 1; i <= 8; i++) {
                total += eval(id[i]) * (9 - i);
            }
            total += eval(id[9]);
            resolve(total % 10 == 0);
        }
    });
};

exports.getUserTicketPriceAsync = function (idcard, callback) {
    checkTwIDAsync(idcard).then(function (isLegal) {
        if (!isLegal) {
            return callback({ err: "身分證字號不合法" });
        }
    }).then(function () {
        UserModel.getUserByIdcard(idcard, function (err, user) {
            if (err) {
                log.error(err);
                return callback({ err: err });
            }

            if (!user) {
                callback({ err: "使用者不存在" });
            } else {
                var initDiscont = 0;
                var userIdCard = user.idcard;
                var userAge = user.age;
                if (userIdCard.charAt(1) == '2') {
                    //女生優惠100塊
                    initDiscont += 100;
                }
                if (userIdCard.charAt(9) % 2 != 0) {
                    //身分證最後一碼為奇數者優惠50塊
                    initDiscont += 50;
                }
                if (userAge > 30) {
                    //年齡超過30優惠80塊
                    initDiscont += 80;
                }
                var ticketPrice = UserModel.getTicketPrice(initDiscont);
                callback({ discount: initDiscont, ticketPrice: ticketPrice });
            }
        });
    });
};