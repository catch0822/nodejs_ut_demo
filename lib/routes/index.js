'use strict';

var _log = require('log');

var _log2 = _interopRequireDefault(_log);

var _path = require('path');

var _path2 = _interopRequireDefault(_path);

var _UserController = require('../controllers/UserController');

var _UserController2 = _interopRequireDefault(_UserController);

var _Constant = require('../constant/Constant');

var _Constant2 = _interopRequireDefault(_Constant);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var log = new _log2.default('myRouters');

_path2.default.join(__dirname, 'models');


exports.homePage = function (req, res) {
  res.render('index.html');
};

exports.getUserTicketPrice = function (req, res) {
  var id = req.param('id');
  _UserController2.default.getUserTicketPrice(id, function (retObj) {
    if (retObj.err) {
      res.send({ err: retObj.err, ticketPrice: null });
    } else {
      res.send({ err: null, ticketPrice: retObj.ticketPrice });
    }
  });
};

exports.getUserTicketPriceAsync = function (req, res) {
  var id = req.param('id');
  _UserController2.default.getUserTicketPriceAsync(id, function (retObj) {
    if (retObj.err) {
      res.send({ err: retObj.err, ticketPrice: null });
    } else {
      res.send({ err: null, ticketPrice: retObj.ticketPrice });
    }
  });
};