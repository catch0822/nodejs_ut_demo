'use strict';

var _chai = require('chai');

var _sinon = require('sinon');

var _sinon2 = _interopRequireDefault(_sinon);

var _UserController = require('../controllers/UserController');

var _UserController2 = _interopRequireDefault(_UserController);

var _index = require('./index');

var _index2 = _interopRequireDefault(_index);

var _Constant = require('../constant/Constant');

var _Constant2 = _interopRequireDefault(_Constant);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

describe('Test Router', function () {
  var userControllerStub = void 0;

  before(function () {
    userControllerStub = _sinon2.default.stub(_UserController2.default, 'getUserTicketPrice');
  });

  after(function () {
    userControllerStub.restore();
  });

  beforeEach(function () {});

  afterEach(function () {});

  it('GET /Homepage', function () {
    var res = { render: _sinon2.default.spy() };
    _index2.default.homePage(null, res);

    // Expectation
    // render 應該被 call 一次
    (0, _chai.assert)(res.render.calledOnce);
    (0, _chai.expect)(res.render.args[0][0]).to.equal('index.html');
  });

  it('GET /getUserTicketPrice, get success', function () {
    // Mock preparation
    var requestBody = { id: 'test' };
    var returnObject = { discount: 180, ticketPrice: _Constant2.default - 180 };
    userControllerStub.withArgs(requestBody.id).yields(returnObject);

    // Trigger sut
    var req = {
      param: function param(_param) {
        return requestBody[_param];
      }
    };

    // spy my send result
    var res = { send: _sinon2.default.spy() };
    _index2.default.getUserTicketPrice(req, res);

    // Expectation
    // send 應該被 call 一次
    console.log(res.send.firstCall.args);
    (0, _chai.assert)(res.send.calledOnce);
    var spyResSend = res.send.firstCall.args[0];
    (0, _chai.expect)(spyResSend.ticketPrice).to.equal(returnObject.ticketPrice);
  });

  it('GET /getUserTicketPrice, get faild', function () {
    // Mock preparation
    var requestBody = { id: 'test' };
    var returnObject = { err: 'test error' };
    userControllerStub.withArgs(requestBody.id).yields(returnObject);

    // Trigger sut
    var req = {
      param: function param(_param2) {
        return requestBody[_param2];
      }
    };

    // spy my send result
    var res = { send: _sinon2.default.spy() };
    _index2.default.getUserTicketPrice(req, res);

    // Expectation
    // send 應該被 call 一次
    (0, _chai.assert)(res.send.calledOnce);
    var spyResSend = res.send.firstCall.args[0];
    (0, _chai.expect)(spyResSend.err).to.equal(returnObject.err);
  });
});