'use strict';

var _express = require('express');

var _express2 = _interopRequireDefault(_express);

var _bodyParser = require('body-parser');

var _bodyParser2 = _interopRequireDefault(_bodyParser);

var _cookieParser = require('cookie-parser');

var _cookieParser2 = _interopRequireDefault(_cookieParser);

var _ejs = require('ejs');

var _ejs2 = _interopRequireDefault(_ejs);

var _path = require('path');

var _path2 = _interopRequireDefault(_path);

var _mongoose = require('mongoose');

var _mongoose2 = _interopRequireDefault(_mongoose);

var _index = require('./routes/index.js');

var _index2 = _interopRequireDefault(_index);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

_mongoose2.default.connect('mongodb://10yearoldpc.ddns.net/utDemo', { useMongoClient: true });
//initialize data
var UserModel = new require(_path2.default.join(__dirname, 'models', 'User.js')).userModel();
UserModel({ name: 'Wei', idcard: 'A197422066', age: 31 }).save();
UserModel({ name: 'Bocheng', idcard: 'A117964743', age: 33 }).save();
UserModel({ name: 'Penny', idcard: 'A203425137', age: 18 }).save();
// global.TICKET_PRICE = 500;
var app = (0, _express2.default)();
var router = (0, _express.Router)();

app.use(_bodyParser2.default.json());
app.use(_bodyParser2.default.urlencoded({ extended: true }));
app.use((0, _cookieParser2.default)());
app.engine('html', _ejs2.default.renderFile);
_ejs2.default.resolveInclude = function (name, filename) {
  var ext = _path2.default.extname(name);
  var includePath = _path2.default.resolve(_path2.default.join(__dirname, 'views'), name);
  if (!ext) {
    includePath += '.html';
  }
  return includePath;
};

router.get('/', _index2.default.homePage);
router.get('/getUserTicketPrice', _index2.default.getUserTicketPrice);
router.get('/getUserTicketPriceAsync', _index2.default.getUserTicketPriceAsync);

app.use('/', router);
module.exports = app;