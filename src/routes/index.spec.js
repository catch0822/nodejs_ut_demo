import { expect, assert } from 'chai';
import sinon from 'sinon';
import UserController from '../controllers/UserController';
import sut from './index';
import TICKET_PRICE from '../constant/Constant';

describe('Test Router', () => {

  it('GET /Homepage', () => {
    const res = { render: sinon.spy() };
    sut.homePage(null, res);

    // Expectation
    // render 應該被 call 一次
    assert(res.render.calledOnce);
    expect(res.render.args[0][0]).to.equal('index.html');
  });

  it('GET /getUserTicketPrice, get success', () => {
    let userControllerStub = sinon.stub(UserController, 'getUserTicketPrice');
    // Mock preparation
    const requestBody = { id: 'test' };
    const returnObject = { discount: 180, ticketPrice: TICKET_PRICE - 180};
    userControllerStub.withArgs(requestBody.id).yields(returnObject);
  
    // Trigger sut
    const req = {
      param: (param) => (requestBody[param]),
    };

    // spy my send result
    let res = { send: sinon.spy(), };
    sut.getUserTicketPrice(req, res);

    // Expectation
    // send 應該被 call 一次
    assert(res.send.calledOnce);
    const spyResSend = res.send.firstCall.args[0];
    expect(spyResSend.ticketPrice).to.equal(returnObject.ticketPrice);
    userControllerStub.restore();
  });

  it('GET /getUserTicketPrice, get faild', () => {
    let userControllerStub = sinon.stub(UserController, 'getUserTicketPrice');
    // Mock preparation
    const requestBody = { id: 'test' };
    const returnObject = { err: 'test error'};
    userControllerStub.withArgs(requestBody.id).yields(returnObject);
  
    // Trigger sut
    const req = {
      param: (param) => (requestBody[param]),
    };

    // spy my send result
    let res = { send: sinon.spy(), };
    sut.getUserTicketPrice(req, res);

    // Expectation
    // send 應該被 call 一次
    assert(res.send.calledOnce);
    const spyResSend = res.send.firstCall.args[0];
    expect(spyResSend.err).to.equal(returnObject.err);
    userControllerStub.restore();
  });

  it('GET /getUserTicketPriceAsync, get success', () => {
    let userControllerStub = sinon.stub(UserController, 'getUserTicketPriceAsync');
    // Mock preparation
    const requestBody = { id: 'test' };
    const returnObject = { discount: 180, ticketPrice: TICKET_PRICE - 180};
    userControllerStub.withArgs(requestBody.id).yields(returnObject);
  
    // Trigger sut
    const req = {
      param: (param) => (requestBody[param]),
    };

    // spy my send result
    let res = { send: sinon.spy(), };
    sut.getUserTicketPriceAsync(req, res);

    // Expectation
    // send 應該被 call 一次
    assert(res.send.calledOnce);
    const spyResSend = res.send.firstCall.args[0];
    expect(spyResSend.ticketPrice).to.equal(returnObject.ticketPrice);
    userControllerStub.restore();
  });

  it('GET /getUserTicketPriceAsync, get faild', () => {
    let userControllerStub = sinon.stub(UserController, 'getUserTicketPriceAsync');
    // Mock preparation
    const requestBody = { id: 'test' };
    const returnObject = { err: 'test error'};
    userControllerStub.withArgs(requestBody.id).yields(returnObject);
  
    // Trigger sut
    const req = {
      param: (param) => (requestBody[param]),
    };

    // spy my send result
    let res = { send: sinon.spy(), };
    sut.getUserTicketPriceAsync(req, res);

    // Expectation
    // send 應該被 call 一次
    assert(res.send.calledOnce);
    const spyResSend = res.send.firstCall.args[0];
    expect(spyResSend.err).to.equal(returnObject.err);
    userControllerStub.restore();
  });

});