import Log from 'log';
let log = new Log('myRouters');
import path from 'path';
path.join(__dirname, 'models')
import UserController from '../controllers/UserController';
import TICKET_PRICE from '../constant/Constant';

exports.homePage = (req, res) => {
  res.render('index.html');
};

exports.getUserTicketPrice = (req, res) => {
  let id = req.param('id');
  UserController.getUserTicketPrice(id, (retObj) => {
    if (retObj.err) {
       res.send({ err: retObj.err, ticketPrice: null });
    } else {
       res.send({ err: null, ticketPrice: retObj.ticketPrice });
    }
  });
};

exports.getUserTicketPriceAsync = (req, res) => {
  let id = req.param('id');
  UserController.getUserTicketPriceAsync(id, (retObj) => {
    if (retObj.err) {
       res.send({ err: retObj.err, ticketPrice: null });
    } else {
       res.send({ err: null, ticketPrice: retObj.ticketPrice });
    }
  });
};
