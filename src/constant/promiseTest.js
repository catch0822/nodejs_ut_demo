exports.promiseFun = () => (
    new Promise((resolve, reject) => {
        resolve('Hello Promise');
    })
);

exports.promiseFun2 = (from1) => (
    new Promise((resolve, reject) => {
        resolve(`promiseFun-----${from1}`);
    })
);
