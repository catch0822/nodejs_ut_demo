import path from 'path';
import promiseTest from '../constant/promiseTest';
// import Log from 'log';
// let log = new Log('UserController.js');
let UserModel = new require(path.join('..', 'models', 'User.js')).userModel();

const checkTwID = (id) => {
    var city = new Array(
        1, 10, 19, 28, 37, 46, 55, 64, 39, 73, 82, 2, 11,
        20, 48, 29, 38, 47, 56, 65, 74, 83, 21, 3, 12, 30
    )
    id = id.toUpperCase();
    if (id.search(/^[A-Z](1|2)\d{8}$/i) == -1) {
        return false;
    } else {
        id = id.split('');
        var total = city[id[0].charCodeAt(0) - 65];
        for (var i = 1; i <= 8; i++) {
            total += eval(id[i]) * (9 - i);
        }
        total += eval(id[9]);
        return total % 10 == 0;
    }
}

exports.getUserTicketPrice = (idcard, callback) => {
    const isLegal = checkTwID(idcard);
    if (!isLegal) {
        return callback({ err: "身分證字號不合法" })
    }

    UserModel.getUserByIdcard(idcard, (err, user) => {
        if (err) {
            // log.error(err);
            return callback({ err });
        }

        if (!user) {
            callback({ err: "使用者不存在" });
        } else {
            let initDiscont = 0;
            let userIdCard = user.idcard;
            let userAge = user.age;
            if (userIdCard.charAt(1) == '2') { //女生優惠100塊
                initDiscont += 100;
            }
            if (userIdCard.charAt(9) % 2 != 0) { //身分證最後一碼為奇數者優惠50塊
                initDiscont += 50;
            }
            if (userAge > 30) {//年齡超過30優惠80塊
                initDiscont += 80;
            }
            const ticketPrice = UserModel.getTicketPrice(initDiscont);
            callback({ discount: initDiscont, ticketPrice });
        }
    });
};

const checkTwIDAsync = (id) => {
    return new Promise((resolve, reject) => {
        var city = new Array(
            1, 10, 19, 28, 37, 46, 55, 64, 39, 73, 82, 2, 11,
            20, 48, 29, 38, 47, 56, 65, 74, 83, 21, 3, 12, 30
        )
        id = id.toUpperCase();
        if (id.search(/^[A-Z](1|2)\d{8}$/i) == -1) {
            resolve(false);
        } else {
            id = id.split('');
            var total = city[id[0].charCodeAt(0) - 65];
            for (var i = 1; i <= 8; i++) {
                total += eval(id[i]) * (9 - i);
            }
            total += eval(id[9]);
            resolve((total % 10 == 0));
        }

    });
}

exports.getUserTicketPriceAsync = (idcard, callback) => {
    checkTwIDAsync(idcard)
        .then((isLegal) => {
            if (!isLegal) {
                return callback({ err: "身分證字號不合法" })
            }
        })
        .then(promiseTest.promiseFun)
        .then(promiseTest.promiseFun2)
        .then(res => {
            // console.log(res);
        })
        .then(() => {
            UserModel.getUserByIdcard(idcard, (err, user) => {
                if (err) {
                    // log.error(err);
                    return callback({ err });
                }

                if (!user) {
                    callback({ err: "使用者不存在" });
                } else {
                    let initDiscont = 0;
                    let userIdCard = user.idcard;
                    let userAge = user.age;
                    if (userIdCard.charAt(1) == '2') { //女生優惠100塊
                        initDiscont += 100;
                    }
                    if (userIdCard.charAt(9) % 2 != 0) { //身分證最後一碼為奇數者優惠50塊
                        initDiscont += 50;
                    }
                    if (userAge > 30) {//年齡超過30優惠80塊
                        initDiscont += 80;
                    }
                    const ticketPrice = UserModel.getTicketPrice(initDiscont);
                    callback({ discount: initDiscont, ticketPrice });
                }
            });
        });
};
