import { expect, assert } from 'chai';
import sinon from 'sinon';
import sut from './UserController';
import promiseTest from '../constant/promiseTest';
let UserModel = new require('../models/User').userModel();

// [Stub][Controller] test
describe('[SUT-Normal][Stub][Controller] test the discount logic', () => {
    let getUserByIdcardStub;
    let getTicketPriceStub;
    
    beforeEach(() => {
        getUserByIdcardStub = sinon.stub(UserModel, 'getUserByIdcard');
        getTicketPriceStub = sinon.stub(UserModel, 'getTicketPrice');
    });

    afterEach(() => {
        getUserByIdcardStub.restore();
        getTicketPriceStub.restore();
    });

    it('Test', () => {
        let promistStub = sinon.stub(promiseTest, 'promiseFun');
        // Mock DB preparation
        const requestBody = { id: 'C258622151' };
        const user = {
            name: 'test_230',
            age: '31',
            idcard: requestBody.id,
        };
        const expectDiscount = 230;
        const expectPrice = 270;
    
        promistStub.resolves('+++++++++++++++++++++++++++++++++');
        getUserByIdcardStub.withArgs(requestBody.id).yields(null, user);
        getTicketPriceStub.returns(expectPrice);
        // Trigger sut
        const spyCallback = sinon.spy();
        sut.getUserTicketPrice(requestBody.id, spyCallback);

        // Expect 1. getTicketPriceStub args should be equal to expectDiscount ($230)
        expect(getTicketPriceStub.firstCall.args[0]).to.equal(expectDiscount);

        // Expect 2. the spyCallback should return the discount and ticketPrice
        const actualCallbackArgs = spyCallback.firstCall.args[0];
        expect(actualCallbackArgs.discount).to.equal(expectDiscount);
        expect(actualCallbackArgs.ticketPrice).to.equal(expectPrice);
       
        promistStub.restore();
    });

    it('1. C258622151, age:31, the discount should be $230', () => {
        // Mock DB preparation
        const requestBody = { id: 'C258622151' };
        const user = {
            name: 'test_230',
            age: '31',
            idcard: requestBody.id,
        };
        const expectDiscount = 230;
        const expectPrice = 270;
        getUserByIdcardStub.withArgs(requestBody.id).yields(null, user);
        getTicketPriceStub.returns(expectPrice);
        // Trigger sut
        const spyCallback = sinon.spy();
        sut.getUserTicketPrice(requestBody.id, spyCallback);

        // Expect 1. getTicketPriceStub args should be equal to expectDiscount ($230)
        expect(getTicketPriceStub.firstCall.args[0]).to.equal(expectDiscount);

        // Expect 2. the spyCallback should return the discount and ticketPrice
        const actualCallbackArgs = spyCallback.firstCall.args[0];
        expect(actualCallbackArgs.discount).to.equal(expectDiscount);
        expect(actualCallbackArgs.ticketPrice).to.equal(expectPrice);
    });

    /**
     * This is for branch 100%,
     * because all the 'else' should be tested although they are not written in the function.
     */
    it('2. A197422066, age:18, the discount should be $0', () => {
        // Mock DB preparation
        const requestBody = { id: 'A197422066' };
        const user = {
            name: 'test_0',
            age: '18',
            idcard: requestBody.id,
        };
        const expectDiscount = 0;
        const expectPrice = 500; // dont care in the test case
        getUserByIdcardStub.withArgs(requestBody.id).yields(null, user);
        getTicketPriceStub.returns(expectPrice);
        // Trigger sut
        const spyCallback = sinon.spy();
        sut.getUserTicketPrice(requestBody.id, spyCallback);

        // Expect 1. getTicketPriceStub args should be equal to expectDiscount ($0)
        expect(getTicketPriceStub.firstCall.args[0]).to.equal(expectDiscount);

        // Expect 2. the spyCallback should return the discount and ticketPrice
        const actualCallbackArgs = spyCallback.firstCall.args[0];
        expect(actualCallbackArgs.discount).to.equal(expectDiscount);
        expect(actualCallbackArgs.ticketPrice).to.equal(expectPrice);
    });

    it('3. F321113052, age:31, 身分證字號不合法', () => {
        // Mock DB preparation
        const requestBody = { id: 'F321113052' };
        const response = '身分證字號不合法';

        // Trigger sut
        const spyCallback = sinon.spy();
        sut.getUserTicketPrice(requestBody.id, spyCallback);
        const actualCallbackArgs = spyCallback.firstCall.args[0];
        expect(actualCallbackArgs.err).to.equal(response);
    });

    it('4. 使用者不存在', () => {
        // Mock DB preparation
        const requestBody = { id: 'C258622151' };
        const expectUser = null;
        const response = '使用者不存在';
        getUserByIdcardStub.withArgs(requestBody.id).yields(null, expectUser);
        // Trigger sut
        const spyCallback = sinon.spy();
        sut.getUserTicketPrice(requestBody.id, spyCallback);
        const actualCallbackArgs = spyCallback.firstCall.args[0];
        expect(actualCallbackArgs.err).to.equal(response);
    });

    it('5. Error', () => {
        // Mock DB preparation
        const requestBody = { id: 'C258622151' };
        const response = 'stub error';
        getUserByIdcardStub.withArgs(requestBody.id).yields(response, {});
        // Trigger sut
        const spyCallback = sinon.spy();
        sut.getUserTicketPrice(requestBody.id, spyCallback);
        const actualCallbackArgs = spyCallback.firstCall.args[0];
        expect(actualCallbackArgs.err).to.equal(response);
    });
});

// [Async][Stub][Controller] test
describe('[SUT-Promise][Stub][Controller] test the discount logic', () => {
    let getUserByIdcardStub;
    let getTicketPriceStub;

    beforeEach(() => {
        getUserByIdcardStub = sinon.stub(UserModel, 'getUserByIdcard');
        getTicketPriceStub = sinon.stub(UserModel, 'getTicketPrice');
    });

    afterEach(() => {
        getUserByIdcardStub.restore();
        getTicketPriceStub.restore();
    });

    it('Test-async', () => {
        let promistStub = sinon.stub(promiseTest, 'promiseFun');

       // Mock DB preparation
        const requestBody = { id: 'C258622151' };
        const user = {
            name: 'test_230',
            age: '31',
            idcard: requestBody.id,
        };
        const expectDiscount = 230;
        const expectPrice = 270;
        getUserByIdcardStub.withArgs(requestBody.id).yields(null, user);
        getTicketPriceStub.returns(expectPrice);
        promistStub.resolves('asyncasyncasyncasyncasync');
        // Trigger sut
        return new Promise((resolve, reject) => {
            sut.getUserTicketPriceAsync(requestBody.id, (res) => {
                resolve(res);
            });
        }).then((res) => {
            // Expect 1. getTicketPriceStub args should be equal to expectDiscount ($230)
            expect(getTicketPriceStub.firstCall.args[0]).to.equal(expectDiscount);

            // Expect 2. the spyCallback should return the discount and ticketPrice
            expect(res.discount).to.equal(expectDiscount);
            expect(res.ticketPrice).to.equal(expectPrice);
       
            promistStub.restore();
        });
    });

    it('1. C258622151, age:31, the discount should be $230', () => {
        // Mock DB preparation
        const requestBody = { id: 'C258622151' };
        const user = {
            name: 'test_230',
            age: '31',
            idcard: requestBody.id,
        };
        const expectDiscount = 230;
        const expectPrice = 270;
        getUserByIdcardStub.withArgs(requestBody.id).yields(null, user);
        getTicketPriceStub.returns(expectPrice);
        // Trigger sut
        return new Promise((resolve, reject) => {
            sut.getUserTicketPriceAsync(requestBody.id, (res) => {
                resolve(res);
            });
        }).then((res) => {
            // Expect 1. getTicketPriceStub args should be equal to expectDiscount ($230)
            expect(getTicketPriceStub.firstCall.args[0]).to.equal(expectDiscount);

            // Expect 2. the spyCallback should return the discount and ticketPrice
            expect(res.discount).to.equal(expectDiscount);
            expect(res.ticketPrice).to.equal(expectPrice);
        });
    });

    it('2. A197422066, age:18, the discount should be $0', () => {
        // Mock DB preparation
        const requestBody = { id: 'A197422066' };
        const user = {
            name: 'test_0',
            age: '18',
            idcard: requestBody.id,
        };
        const expectDiscount = 0;
        const expectPrice = 500; // dont care in the test case
        getUserByIdcardStub.withArgs(requestBody.id).yields(null, user);
        getTicketPriceStub.returns(expectPrice);
        // Trigger sut
        return new Promise((resolve, reject) => {
            sut.getUserTicketPriceAsync(requestBody.id, (res) => {
                resolve(res);
            });
        }).then((res) => {
            // Expect 1. getTicketPriceStub args should be equal to expectDiscount ($230)
            expect(getTicketPriceStub.firstCall.args[0]).to.equal(expectDiscount);

            // Expect 2. the spyCallback should return the discount and ticketPrice
            expect(res.discount).to.equal(expectDiscount);
            expect(res.ticketPrice).to.equal(expectPrice);
        });
    });

    it('2. F321113052, age:31, 身分證字號不合法', () => {
        // Mock DB preparation
        const requestBody = { id: 'F321113052' };
        const response = '身分證字號不合法';

        // Trigger sut
        return new Promise((resolve, reject) => {
            sut.getUserTicketPriceAsync(requestBody.id, (res) => {
                resolve(res);
            });
        }).then((res) => {
            expect(res.err).to.equal(response);
        });
    });

    it('3. 使用者不存在', () => {
        // Mock DB preparation
        const requestBody = { id: 'C258622151' };
        const expectUser = null;
        const response = '使用者不存在';
        getUserByIdcardStub.withArgs(requestBody.id).yields(null, expectUser);
        // Trigger sut
        return new Promise((resolve, reject) => {
            sut.getUserTicketPriceAsync(requestBody.id, (res) => {
                resolve(res);
            });
        }).then((res) => {
            expect(res.err).to.equal(response);
        });
    });

    it('4. Error', () => {
        // Mock DB preparation
        const requestBody = { id: 'C258622151' };
        const err = 'stub error';
        getUserByIdcardStub.withArgs(requestBody.id).yields(err, {});
        // Trigger sut
        return new Promise((resolve, reject) => {
            sut.getUserTicketPriceAsync(requestBody.id, (res) => {
                resolve(res);
            });
        }).then((res) => {
            expect(res.err).to.equal(err);
        });
    });
});


// [Mock][Controller] test
describe('[SUT-Normal][Mock][Controller] test the discount logic', () => {

    it('1. C258622151, age:31, the discount should be $230', () => {
        const userModelMock = sinon.mock(UserModel);
        // Mock DB preparation
        const requestBody = { id: 'C258622151' };
        const user = {
            name: 'test_230',
            age: '31',
            idcard: requestBody.id,
        };
        const expectDiscount = 230;
        const expectPrice = 270;
        userModelMock.expects('getUserByIdcard').withArgs(requestBody.id).yields(null, user);
        userModelMock.expects('getTicketPrice').once().withArgs(expectDiscount).returns(expectPrice);
        // Trigger sut
        const spyCallback = sinon.spy();
        sut.getUserTicketPrice(requestBody.id, spyCallback);

        // Expect 1. getTicketPrice args should be equal to expectDiscount ($230) and executed once time
        userModelMock.verify();
        userModelMock.restore();

        // Expect 2. the spyCallback should return the discount and ticketPrice
        const actualCallbackArgs = spyCallback.firstCall.args[0];
        expect(actualCallbackArgs.discount).to.equal(expectDiscount);
        expect(actualCallbackArgs.ticketPrice).to.equal(expectPrice);
        
    });
});

// [Mock][Controller] test
describe('[SUT-Promise][Async][Mock][Controller] test the discount logic', () => {

    it('1. C258622151, age:31, the discount should be $230', () => {
        const userModelMock = sinon.mock(UserModel);
        // Mock DB preparation
        const requestBody = { id: 'C258622151' };
        const user = {
            name: 'test_230',
            age: '31',
            idcard: requestBody.id,
        };
        const expectDiscount = 230;
        const expectPrice = 270;
        userModelMock.expects('getUserByIdcard').withArgs(requestBody.id).yields(null, user);
        userModelMock.expects('getTicketPrice').once().withArgs(expectDiscount).returns(expectPrice);

        // Trigger sut
        return new Promise((resolve, reject) => {
            sut.getUserTicketPriceAsync(requestBody.id, (res) => {
                resolve(res);
            });
        }).then((res) => {
            // Expect 1. getTicketPriceStub args should be equal to expectDiscount ($230)
            userModelMock.verify();
            userModelMock.restore();

            // Expect 2. the spyCallback should return the discount and ticketPrice
            expect(res.discount).to.equal(expectDiscount);
            expect(res.ticketPrice).to.equal(expectPrice);
        });
    });
});
