import Log from 'log';
let log = new Log('userModel.js');
import mongoose from 'mongoose';
import TICKET_PRICE from '../constant/Constant';
let Schema = mongoose.Schema;

var userSchema = new Schema({
    name: {
        type: String,
        required: true
    },
    idcard: {
        type: String,
        index: {unique: true, dropDups: true},
        required: true
    },
    age: {
        type: Number,
        required: true
    }
});

userSchema.static('getUserByIdcard', (idcard, callback) => {
    UserModel.findOne({
        idcard: idcard
    }, function(err, user) {
        callback(err, user);
    });
});

userSchema.static('getTicketPrice', (discount) => (TICKET_PRICE - discount));

var UserModel = mongoose.model('user', userSchema);

function User() {

}

User.userModel = () => {
    return UserModel;
}

module.exports = User;