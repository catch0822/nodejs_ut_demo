import { expect, assert } from 'chai';
import sinon from 'sinon';
import path from 'path';
import mongoose from 'mongoose';
import TICKET_PRICE from '../constant/Constant';
let sut = new require('./User').userModel();

describe('Test DB', () => {
    let userFindOneMock;
    const testUser = {
        name: 'test',
        age: '20',
        idcard: 'C221115555',
    };

    before(() => {
        userFindOneMock = sinon.stub(mongoose.Model, 'findOne');
    });

    after(() => {
        userFindOneMock.restore();
    });

    beforeEach(() => {
    });

    afterEach(() => {
    });

    it('mongo.getUserByIdcard, should return user name and age.', () => {
        // Mock prepare
        userFindOneMock.withArgs({ idcard: testUser.idcard }).yields(null, testUser);
        // Trigger sut
        let callback = sinon.spy();
        sut.getUserByIdcard(testUser.idcard, callback);
        // Expectation
        assert(callback.calledOnce);
        const spyUSER = callback.firstCall.args[1];
        expect(spyUSER.name).to.equal(testUser.name);
        expect(spyUSER.age).to.equal(testUser.age);
    });

    it('mongo.getTicketPrice, should return the final price.', () => {
        const discount = 100;
        const result = sut.getTicketPrice(discount);
        // Expectation
        expect(result).to.equal(TICKET_PRICE - discount);
    });
});