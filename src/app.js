import express, { Router } from 'express';
import bodyParser from 'body-parser';
import cookieParser from 'cookie-parser';
import ejs from 'ejs';
import path from 'path';
import mongoose from 'mongoose';
mongoose.connect('mongodb://10yearoldpc.ddns.net/utDemo',{useMongoClient:true});
//initialize data
let UserModel = new require(path.join(__dirname, 'models', 'User.js')).userModel();
UserModel({name: 'Wei', idcard: 'A197422066', age: 31}).save();
UserModel({name: 'Bocheng', idcard: 'A117964743', age: 33}).save();
UserModel({name: 'Penny', idcard: 'A203425137', age: 18}).save();
// global.TICKET_PRICE = 500;
let app = express();
let router = Router();

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));
app.use(cookieParser());
app.engine('html', ejs.renderFile);
ejs.resolveInclude = (name, filename) => {
  var ext = path.extname(name);
  var includePath = path.resolve(path.join(__dirname, 'views'), name)
  if (!ext) {
    includePath += '.html';
  }
  return includePath;
};

import myRouters from "./routes/index.js"

router.get('/', myRouters.homePage);
router.get('/getUserTicketPrice', myRouters.getUserTicketPrice);
router.get('/getUserTicketPriceAsync', myRouters.getUserTicketPriceAsync);

app.use('/', router);
module.exports = app;
