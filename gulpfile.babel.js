import gulp from 'gulp';
import babel from 'gulp-babel';
import nodemon from 'gulp-nodemon';
import livereload from 'gulp-livereload';

gulp.task('build', function () {
    return gulp.src("src/**/*.js")
        .pipe(babel())
        .pipe(gulp.dest("lib"))
        .pipe(livereload());
});

gulp.task('watch', ['build'], function () {
    livereload.listen();
    gulp.watch([
      'src/conf/**/*.js',
      'src/routes/**/*.js',
      'src/models/**/*.js',
      'src/controllers/**/*.js',
      'src/views/**/*.html',
      'src/**/*.js'
    ], ['build']);
    nodemon({
      script: "./bin/www",
      ext: "js"
    });
});

gulp.task('default', ['watch']);